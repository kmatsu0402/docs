# Agenda

* What is software architecture
* How does software architecture fit in the big picture?
* Architectural pattern vs design pattern
* Various architectural patterns

# What is Software Architecture / How Does Software Architecture Fit in the Big Picture?

The ~~design output~~ **software architecture** is the **input of the development phase**.

Simple software development flow

```
Requirements > Analysis > Design > Development > Testing > Acceptance > Deployment > Maintenance and Update
```

Software architecture is...

1.  Architectural Patterns

* how the defining components of a software system are organized and assembled ~~built~~.

2.  Messaging and APIs

* how the defining components communicate with each other

3.  Quality Attributes (e.g. scalability, security, etc...)

* constraints the whole system is ruled by

_Knowing the architectural patter will help make good decisions in the development phase._

# Architecture vs Design Patterns

| Architecture Pattern                           | Design Pattern                 |
| ---------------------------------------------- | ------------------------------ |
| high-level, universal scope                    | lower-level, development scope |
| how components are **organized and assembled** | how components are **built**   |

| Levels  | Attributes                             |
| ------- | -------------------------------------- |
| Level 1 | Monolithic, Service Based, Distributed |
| Level 2 | Layered, Micro Services, Event Driven  |

# Architecture Pattern

## Layered Architectural Pattern

* also known as N-tier pattern
* monolithic
* separation of concerns
* layers as major components

Presentation < Business < Persistence < Infrastructure

## Micro Services Architectural Pattern

* service-based type of pattern
* independently evolve-able and deploy-able

Client < API Gateway <=> Service A, Service B, ...

## Event Driven Pattern

* distrubuted architecture

- event processing units coordinated in a **mediator** or **broker** topology
- asynchronous nature
- overall structure depends on the topology chosen

## Micro Kernel Architectural Pattern

* also known as the plug-in architectural pattern
* major components:
  * core system (like a kernel)
  * plug-ins
* monolithic
* e.g. browser, text editors, OS

## Service-Oriented Architectural Pattern

* large enterprise systems
* coarse grained services
* integrating different heterogeneous components together

## Space-Based Architectural Pattern

* hybrid of architectural patterns
* inspired by space tuples
* distributed caching - in memory data grid

# Resources

* [(Youtube) Software Architecture | Architectural patterns | Architecture vs Design pattern
  ](https://www.youtube.com/watch?v=lTkL1oIMiaU)
* Software Architecture in Practice
* Pattern-Oriented Software Architecture
